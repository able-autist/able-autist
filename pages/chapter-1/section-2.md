---
path: "/introduction/what-is-autism"
date: "2018-08-25"
title: "What is autism?"
chapter: 1
section: 2
previous: "/introduction"
---

When reading almost any academic journal article about autism you'll find a definition of it that reads more or less like this:

>>>
Autism is a pervasive developmental disorder defined by constrained socialization, repetitive behavior, and usually accompanied by a nubmer of other symptoms.
>>>

However, that description is a far cry from the reality of being autistic. While the above statement is true, it misses many important aspects of being autistic such as sensitivty to a wide range of stimuli and an inability to easily understand one's own emotions. Ultimately, it's a definition meant for diagnosis and clinical understanding whereas we want to be able to understand from the inside what is going on.

Thankfully, new techniques and research have led to a deepening understanding of autism at the neurological level, and it's becoming clear that autism involves deep fundamental structural changes in the brain[^1]. These include changes in brain volume, connectivity, and structure. The changes invovled are significant enough that researches have developed a methodology using artificial intelligence and brain wave measurements to see if someone is autistic. [^2]

These changes have real world consequences to the way that autistic people think and perceieve the world. Here's an example from a fellow autist:

>>>
My wife has a good sized group of friends who like to have parties every now and again. They're all good people and I like them for the most part and I'd like to enjoy the parties too, but I can't. I find the constant chatter, laughter, lots of things going on to be quite painful. It's physically painful to me to stay at the parties for long before my ears start to hurt and i become agitated and overwhelmed.
>>>

Too many (if not most) people the idea that fairly typical levels of sound could be physical painful is an idea that could be hard to grasp. Beyond that, many autists often seem to have a head full of thoughts, so many thoughts that it's very hard to tell what any one thought is in particular. From another autist:

>>>
The quote goes here
>>>

Whether it is a head full of thoughts, sensitivity to stimuli, or a myriad of other conditions, autists find themselves facing a number of challenges that their neurotypical (NT) peers do not typically have. Despite this, the typical course of action with many autists is to try and take these differences and somehow convert them to how they would be in a neurotypical person. Yet, since these differences come from brain structure and aren't a result of psychology it's a strategy that a worst fails catastrophically and at best ends with the autistic person trying to "act neurotypical". While many consider this a success, they often do not realize the cost it takes to put up an act around others on a continual basis.

This picture might seem bleak, and to a certain degree it is. However, that is just the picture today, and by improving our understanding and awareness we can change course.

In truth, the solution to these issues is not to become less autistic, but more autistic. While this may seem counter-intuitive it really is the only choice. The changes to the brain in autistic people are low level, they are physical and chemical changes. Trying to undo this is an essentially impossible task.

Not only is it an impossible task, but it's an unecessary one. I have found (along with other autists) that with practice and training most of the issues that autism is known for slowly fade away. By becoming "more autistic" you will find that the anxiety you have will decrease, you'll find that being around people will slowly become more easy and you'll gain greater comfort in social situations. Perhaps most importantly, you'll find that it's easier to be yourself and rather than simply pretending to be "normal" you can show others who you are.

This will not solve everything, and in the short term it can even mean that your difficulties will increase. But it's much better to bear temporary discomfort than a lifetime of living beyond the boundaries of society. We are ultimately a minority, and for any minority group it is difficult to make your voice heard. This is especially true when finding that voice is difficult in the first place.

I think that it's essential that this voice be heard. Autists are among the most open-minded and free thinking among us. More than ever humanity needs creative solutions to tricky problems, and to be able to bring together others with disparate ways of thinking. It's a role that we are perfectly positioned to play, and one that we have before.

<div class="foot-notes">

[^1] https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4688328/
[^2] https://www.nature.com/articles/s41598-018-24318-x

</div>