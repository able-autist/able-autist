module.exports = {
  siteMetadata: {
    title: 'Able Autist',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/pages`,
        name: "markdown-pages",
      },
    },
    'gatsby-transformer-remark'
  ],
}
