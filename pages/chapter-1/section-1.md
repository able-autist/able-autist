---
path: "/introduction"
date: "2018-08-25"
title: "Introduction"
chapter: 1
section: 1
next: "/introduction/what-is-autism"
---

Like many stories, this one starts with hitting the lowest of lows. A point at which your will to continue existing is tested, and your ability to keep it together mentally is thrown in the deep end. Like a whirlwind a perfect storm of factors had come together to bring me to me knees.

At the top of the list though, was a strange condition that would at times cause me to feel nauseated, only for it to disipate with time. Followed, by a *brain fog* in which the ability to grasp and use my thoughts left me feeling that my mind was trudging through the mud. Or my surprising inability to read the meaning of many things that people did around me. It felt like living with a curse.

As it turns out, these things were happening because I'm autistic, a fact that I wouldn't become aware of until later. At the time, because I didn't know what was going on I started searching in every possible place for clues and techniques to help address these mysterious problems. I began reading books on spirituality, I took psychedelic drugs, I took up yoga, and I started practicing the martial arts. Things began to look up.

Then one day, I followed a link and decided the read the Wikipedia Article on Autism[^1]. I wasn't expecting to find and answer, but when reading through the typical symptoms and traits I immediately noticed the similarties. In the months that followed, I would spend endless hours reading research papers, finding anecdotes, and connecting with other autists like myself.

---

What I found was both exciting and saddening. On one hand, I had found an amazing slice of humanity that has seemingly and generally gone unnoticed. On the other hand, many of these people were living and continue to live a rough existence, shunted to the edges of society.

Indeed I had been living with a curse. However, my curse had not been that I am autistic, rather it had been my *ignorance* that had been my curse. With the curse lifted, I have been able to see and explore life in new ways, and gain new insight on what it means to be alive.

As I mentioned before, I didn't set out to find that I was autistic, rather I set out to try and help myself. It turns out that many of the problems autists experience are due to ignorance, and not directly due to the condition itself. As a result, through trial and error (combined with research) I've found ways to deal with most of the aspects of being autistic.

My intention is to help spread this knowledge. Everyone deserves a fair chance at a rewarding and fufilling life. There are too many people like myself that currently do not have that chance. However, armed with a little knowledge and appropriate techniques it is possible to radically change how we feel and think.

I hope you find this useful!

*The Able Autist*




<div class="foot-notes">

[^1] https://en.wikipedia.org/wiki/Autism

</div>