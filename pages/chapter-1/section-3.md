---
path: "/return-to-the-forest"
date: "2018-08-28"
title: "Return to the Forest"
chapter: 2
section: 1
previous: "/introduction"
---

Contrary to popular thought today, Austim has been something that has been with us for a long time. While it's become popular to ascribe the label to various historical figures (though is does seem likely in some cases such as Henry Cavedendish[^1]), it is something that goes back further than the historical period. Recent research has indicated[^2] that genetic changes that separated us from Neanderthals also likely gave rise to autism and other related conditions. Interestingly enough then it seems, being autistic is deeply tied up in being human.

So then, where are all the autists in history? It seems quite possible (perhaps even likely), that autists fufilled various magical-religious roles in early human groups [^3] such as being a shaman. Indeed, even the progression of autism has very interesting parallels to the "shaman's sickness" [^4] which was said to effect people heading into puberty and often taking years or sometimes decades to cure.

The parallels do not stop there. One of the key aspects of shamanism is healing with plants. This is a difficult task, in large part it requires a massive memory to be able to retain the thousands of possible remedies, where they can be found, when they can be found, and any potential combinations thereof. A study investigating the integration of those with autistic traits in early human groups recounts the following story: [^5]

>>>
In his study of Siberian reindeer herders for example Vitebsky describes ‘old grandfather’, an individual who had a detailed memory of the parentage, medical history and character of each one of the 2,600 reindeer in the herd, vital knowledge which made a significant contribution to their management and survival. Old grandfather was more comfortable in the company of reindeer than of humans, but was much respected and had a wife, son and grandchildren...
>>>

Today, shamanism is a mostly dying instituion, and is quickly becoming a relic of early human history. Yet it is in shamanism where we find some of the best clues that are able to help us. To our ancestors they were treating a condition that was mysterious and that they did not know the underlying mechanism of. With experimentation and time it seems they were able to find ways in which this sickness could be healed.

So then, in order to help ourselves, we must go backwards. We must "Return to the Forests" of our ancestors and use their knowledge and wisdom. Where we do have a sizable leg up on them is that we now have an idea of what we are dealing with. It is no longer the mysterious sickness of our ancestors, but the known condition of autism. This allows us to take methods that were developed long ago, and adapt them to modern usage supported by research and scientific understanding.

So that's the good news. The bad news is that this is a difficult process, and depending on where you are in life it could take years to fully go through. Nonetheless, while you do this it will get better and over time you'll find that the process builds on itself.

So what were the techniques that our prehistoric ancestors used to treat this mysterious sickness?

1. Physical conditioning and self care
2. Exposure to extreme and exotic conditions
3. Use of psychedelic drugs when available
4. Rhythmic practice such as drumming and dancing
5. Study and contemplation
6. Artistic and other forms of expression

Perhaps not surprisingly, all of these techniques have shown promise in the modern day. Exposure to extreme conditions is today what we call exposure therapy. Psychedelics have anecdotal evidence in reducing autistic symtoms [^6]. Rhythmic motion is something that most autists will know as something that can be helpful in calming the nerves.

By adopting these things you are not adopting a cure so much as you are adopting the autistic lifestyle. We all too often end up living the lifestyle of the neurotypical people around us, and more than anything it seems this is the source of many of our problems. Simply put, we need to live in a different way from most people. When we live in accordance with this way, our problems tend to go away. When we stray from this path, we find the walls closing in on us yet again.

We are lucky in that this path helps lessen the problems that plague us, but are also things that turn us into good people. Becoming "more autistic" is the same road to become a better human. For the neurotypical of the world, it will be a matter of choice whether to follow this path or not. For the autistic, it could very well be a matter of life and death. Regardless of where you are right now I urge you to choose life and I urge you to follow these things.

Adopting a lifestyle is not an easy changes, and it's not something that is going to happen all at once. It is something that you will have to come back to again and again.

We will go through each of these things in turn. We will talk about the benefits and potential downsides of each part. Finally, we will do our best to integrate these disparate parts into a whole.

<div class="foot-notes">

[^1] https://en.wikipedia.org/wiki/Henry_Cavendish
[^2] https://www.nature.com/articles/nature19075
[^3] https://the-art-of-autism.com/an-autistic-shaman-shares-why-autistic-people-make-good-shamans/
[^4] https://en.wikipedia.org/wiki/Shamanism#Initiation_and_learning
[^5] https://www.tandfonline.com/doi/full/10.1080/1751696X.2016.1244949
[^6] https://www.reddit.com/r/LSD/comments/36yz1e/have_autism_tried_lsd/

</div>