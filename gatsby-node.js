/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const path = require("path");


const chapters = {
    1: 'Introduction'
}

exports.createPages = ({ boundActionCreators, graphql }) => {
    const { createPage } = boundActionCreators;

    const pageTemplate = path.resolve(`src/templates/pageTemplate.js`);

    return graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
      ) {
        edges {
          node {
            frontmatter {
              path
              title
              chapter
              section
            }
          }
        }
      }
    }
  `).then(result => {
            if (result.errors) {
                return Promise.reject(result.errors);
            }


            result.data.allMarkdownRemark.edges.forEach(({ node }) => {
                const whitespaceRegex = /[\s]/gi
                const specialCharRegex = /[^\-]/gi
                var chapterNumber = node.frontmatter.chapter

                // Format Path For Sections
                var path = '/' + chapters[chapterNumber].toLowerCase().replace(whitespaceRegex, '-')
                if (node.frontmatter.section != 1) {
                    path += '/' + node.frontmatter.title.toLowerCase().replace(whitespaceRegex, '-')
                }

                createPage({
                    path: path,
                    component: pageTemplate,
                    context: {}, // additional data can be passed via context
                });
            });
        });
};