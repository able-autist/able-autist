import React from 'react'
import './page.css'

export default function Template({
  data,
}) {
    // Get post content
    const { markdownRemark } = data;
    const { frontmatter, html } = markdownRemark

    // Create previous and next links
    const prevLink = <div></div>
    const nextLink = <div></div>
    return (
      <div className="content-container">
        <div className="content-main">

          <h2 className="title">{frontmatter.title}</h2>
          <div className="section-container">
            <h2 className="chapter">Chapter {frontmatter.chapter}</h2>
            <div className="separator"></div>
            <h2 className="section">Section {frontmatter.section}</h2>
          </div>
          <div
            className="blog-post-content"
            dangerouslySetInnerHTML={{ __html: html }}
          />
          <div className="navigation">
            {prevLink}
            {nextLink}
          </div>
        </div>
      </div>
    );
}

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        path
        title
        chapter
        section
      }
    }
  }
`;
