---
path: "/exercise"
date: "2018-08-28"
title: "Exercise and self care"
chapter: 2
section: 1
previous: "/introduction"
---

>>>
No citizen has a right to be an amateur in the matter of physical training…what a disgrace it is for a man to grow old without ever seeing the beauty and strength of which his body is capable.

- Socrates
>>>

This section may be the most important of this entire book. If you are to take any advice contained herein, take the advice of this chapter. The reasoning is simple (and because it's simple I won't attempt to provide greater evidence), when faced with high sensitivity any feelings in the body are going to be magnified. While autistic people are often unable to easily interpret these feelings it does not make their existence any less real.

Moreover, it seems that a lack of rigourous exercise leads to many of the physical behaviors we associate with autism. I've found (and observed in others) that adopting this will on its own greatly reduce stimming, anxiety, and will help promote a general feeling of wellbeing. These changes alone can have a massive impact on your life and are why exercise is so important.

I'll say flat out that every autist I know that is doing well has adopted some form of rigourous exercise and practices it in a nearly religious manner. For myself, it has been kickboxing. I've seen others adopt swimming, cylcing, and powerlifting. Regardless, the sky is the limit as there are many ways to exercise.

For yourself, you should adopt anything you have a good interest in, the more interest the better. I highly suggest trying out a wide range of physical activities to find ones your enjoy and suit your personality. Outside of this, the main requirement is that it should be physically very rigourous and you should be able to tire yourself out every time you do the exercise.

The end goal should be this, to be able to intensely exercise five days a week for an hour to an hour and a half. This might seem daunting, especially if you are out of shape. This is the situation I was in myself when I was started. Don't be disheartened, this is something that takes time and progress is not linear. If you are out of shape it may take years to get to this point (it took me about 2 years).

If you have been sedentary, a good start is to simply to try to start going for walks on a regular basis. This alone has incredible mental and physical benefits that can be realized simply by doing it over the course of a few months. 

I can offer you no direct path here, but again this is something you should consider an absolute necessity to your ongoing wellbeing. While most other things throughout this book are negotiable this one is not. Find a physical activity to adopt and adopt it like you have adopted nothing before!





